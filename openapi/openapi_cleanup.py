import re
from bs4 import BeautifulSoup, Comment, NavigableString
from copy import copy

# Small helper-func that can be used to generate web-indexable-anchors.
def _header_id(text):
    return text.replace(' ', '-').lower().strip()

# Open file to be manipulated.
with open('index.html', 'r') as f:
    contents = f.read()
soup = BeautifulSoup(contents, 'html5lib')

# Find first h1 and all .app-desc and nuke them
h1 = soup.find('h1', text=re.compile('OpenAPI definition'))
h1.decompose()
for unwanted in soup.find_all('div', attrs={'class': 'app-desc'}):
    unwanted.decompose()
for unwanted in soup.find_all('div', attrs={'class': 'license-info'}):
    unwanted.decompose()
for unwanted in soup.find_all('div', attrs={'class': 'license-url'}):
    unwanted.decompose()

# Find all jump's and nuke them.
jump = soup.find_all('a', attrs={'class': 'up'})
for unwanted in jump:
    unwanted.decompose()

# Find all [  Jump to <a href='#__*'>*</a>  ] elements and nuke them.
for unwanted_jump in soup.find_all(text=re.compile(r'\[ Jump to ')):
    sibling1 = unwanted_jump.next_sibling
    sibling2 = sibling1.next_sibling
    if sibling1.name == 'a' and sibling2.name == None:
        unwanted_jump.extract()
        sibling1.decompose()
        sibling2.extract()

# Find all comments and nuke them
for comments in soup.findAll(text=lambda text:isinstance(text, Comment)):
    comments.extract()

# Remove both table of contents sections.
method_summary_div = soup.find('div', attrs={'class': 'method-summary'})
for toc in soup.find_all('h3', text=re.compile('Table of Contents')):
    el = toc
    included = [el, method_summary_div]

    while True:
        if not el:
            break
        if el not in included and el.name not in [None, 'h4', 'ul', 'ol']:
            break

        old_el = el
        el = el.next_sibling
        old_el.extract()

# Wrapping all h2s and their content in a <section class='jump'/>
# Get the first h2, add a section above it, then move all content from the first
# h2 to the last .model into the section.
top_h2 = soup.find('h2')
last_model = soup.find_all('div', attrs={'class': 'model'})
wrapper = soup.new_tag('section', attrs={'class': 'jump'})

el = top_h2
while True:
    next_el = el.next_sibling
    el.wrap(wrapper)
    if el == last_model:
        break

    el = next_el
    if not el:
        break

# Find h2 with content 'Access' and remove it.
access = soup.find(lambda elm: elm.name == "h2" and "Access" in elm.text)
access.extract()

# Find all h1 tags and replace with h3
h3 = soup.find_all('h3')
for header in h3:
    header.name = 'h4'
h1 = soup.find_all('h1')
for header in h1:
    header.name = 'h3'
models = soup.find_all('div', attrs={'class': 'model'})
for model in models:
    model.find('h4').name = 'h3'

# Find all content type examples and remove
for unwanted in soup.find_all('div', attrs={'class': 'example-data-content-type'}):
    unwanted.decompose()

# Find all headers and add IDs to them
for header in soup.find_all('h1'):
    header.attrs['id'] = _header_id(header.text)
for header in soup.find_all('h2'):
    header.attrs['id'] = _header_id(header.text)
for header in soup.find_all('h3'):
    header.attrs['id'] = _header_id(header.text)

# Remove all unwanted headers from the output.
for unwanted in soup.find_all('h4', text=re.compile('Produces')):
    unwanted.decompose()
for unwanted in soup.find_all('span', text=re.compile('Accept')):
    unwanted.decompose()
for unwanted in soup.find_all('span', text=re.compile('Content-Type')):
    unwanted.decompose()
for unwanted in soup.find_all('h4', text=re.compile('Consumes')):
    unwanted.decompose()
for unwanted in soup.find_all('h4', text=re.compile('Example data')):
    if unwanted.next_sibling.name == 'pre':
        unwanted.next_sibling.decompose()
    unwanted.decompose()

# Remove all unwanted example text.
for unwanted in soup.find_all('pre', attrs={'class': 'example'}):
    line1 = unwanted.next_sibling.next_sibling
    line2 = unwanted.next_sibling.next_sibling.next_sibling
    line3 = unwanted.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling
    line1.extract()
    line2.extract()
    line2.extract()

# Remove all unwanted endpoints.
headers = [
    "body_1-", 
    "body-", 
    "cssdata---the-merchant's-custom-css-styles.-", 
    'wechatpay-'
]

# Change type of h4->h5 for tocbot to index.
for method in soup.find_all('h4', attrs={'class': 'field-label'}):
    method.name = 'h5'

# Change type of `.method-path` for tocbot to index.
for method in soup.find_all('div', attrs={'class': 'method-path'}):
    method.name = 'h4'
    url_text = _header_id(method.find('code').text)
    method.attrs['id'] = url_text
    method.wrap(soup.new_tag('a', href= '#' + url_text))
    method.a.extract();
    method.pre.code.unwrap();

# remove endpoints
endpoints = [
             '#get-/v1/properties/system'
            ]

for endpoint in endpoints:
    endpoint = soup.find('a', {'href': endpoint})
    if not endpoint:
        continue
    endpoint.parent.extract()

# remove controllers and requests
ids = [
    'propertiescontroller'
]

for id in ids: 
    id = soup.find('h3', {'id': id})
    if not id:
        continue
    id.extract()

# remove models
models = [
    'propertiesresponsemodel----'
]

for model in models: 
    model = soup.find('h3', {'id': model})
    if not model:
        continue
    model.parent.extract()

# Change header link name
names = {
    'applepaycontroller': 'Apple Pay',
    'basketcontrollerv1': 'Basket',
    'basketmigrationcontroller': 'Basket Migration',
    'googlepaycontroller': 'Google Pay',
    'hirepurchaseddcontroller': 'Hire Purchase Debit',
    'installmentsecuredcontroller': 'Installment Secured',
    'invoicefactoringcontroller': 'Invoice Factoring',
    'invoiceguaranteedcontroller': 'Invoice Guaranteed',
    'invoicesecuredcontroller': 'Invoice Secured',
    'merchantkeypaircontroller': 'Merchant Keypair',
    'merchantpaymentpagecontroller': 'Merchant Payment Page',
    'paypalcontroller': 'PayPal',
    'pfcardcontroller': 'Post Finance Card',
    'pfefinancecontroller': 'Post Finance Efinance',
    'piscontroller': 'PIS',
    'sepadirectdebitcontroller': 'Sepa Direct Debit',
    'sepadirectdebitguaranteedcontroller': 'Sepa DirectDebit Guaranteed',
    'sepadirectdebitsecuredcontroller': 'Sepa Direct Debit Secured',
    'simualationasynccontroller': 'Simualation Async',
    'webhookregistrationcontroller': 'Webhook Registration',
    'wechatpaycontroller': 'WeChat Pay'
}

for controller_name, name_humanize in names.items(): 
    controller_name = soup.find('h3', {'id': controller_name})
    if not controller_name:
        continue
    controller_name.a.string = name_humanize

# Change model headers to links
for model in soup.find_all('div', attrs={'class': 'model'}):
    if not model.h3:
        continue
    model.h3.a.unwrap()
    text = model.h3.code.text
    model.h3.code.unwrap()
    model.h3.string = text
    model.h3.attrs['id'] = text
    model.h3.wrap(soup.new_tag('a', href= '#' + text))

# Wrap endpoints by method
methods = ''
for children in soup.find('section', attrs={'class': 'jump'}).children:
    if type(children) == NavigableString:
        continue

    if not children.get('class') == ['method']:
        methods = soup.new_tag('div', attrs={'class': 'endpoints'})
        continue

    children.wrap(methods)

# Remove return type
for return_type_text in soup.find_all("h5", string='Return type'):
    return_type_text.extract()
for return_type in soup.find_all("div", attrs={'class': 'return-type'}):
    return_type.extract()

# Sort endpoints by method
for endpoints in soup.find_all('div', attrs={ 'class': 'endpoints' }):
    sorted_by_method = sorted(endpoints, key=lambda e: e.find('span', attrs={"class": "http-method"}).string)
    for endpoint in sorted_by_method:
        endpoints.append(endpoint)

# Move models description to endpoints
models = soup.find_all('div', attrs={'class': 'model'})
for model in models:
    if type(model) == NavigableString:
        continue
    # prepair models first
    if model.a['href']:
        for model_discription in model.find_all('div', attrs={'class': 'param-desc'}):
            model_name = model_discription.span.a['href'][1:]
            model_h3 = soup.find('h3', attrs={'id': model_name })
            if model_h3:
                model_copy = copy(model_h3.parent.parent.find('div', attrs={'field-items'}))
                model_discription.span.extract()
                model_discription.append(model_copy)
        
        # copy models to endpoints
        methods = soup.find_all('div', attrs={'class': 'method'})
        for method in methods:
            for endpoint in method.find_all('a', attrs={'href': model.a['href'] }):
                if endpoint:
                    if endpoint == model.a:
                        continue
                    model_copy = copy(model)
                    if model_copy.a.h3:
                        model_copy.a.extract()
                        endpoint.replaceWith(model_copy)

# delete models after everything is moved
soup.find('h2', attrs={'id': 'models'}).extract()

for model in models:
    model.extract()

# change parameter type
for param_type in soup.find_all('span', attrs={'class': 'param-type'}):
    if param_type.a:
        param_type.a.unwrap()

# change class for first level of field items div
for model in soup.find_all('div', attrs={'class': 'model'}):
    field_items = model.find('div', attrs={'class': 'field-items'})
    field_items['class'] = 'root-field-items'

# style changes for items list
for param_type in soup.find_all('div', attrs={'class': 'param-desc'}):
    param_name = param_type.find_previous_sibling('div', attrs={'class': "param"})
    param_type.name = 'span'
    param_name.append(' ')
    param_name.append(param_type)
    sub_items = param_name.div
    if sub_items:
        param_name.append(sub_items)
    param_type.name = 'small'

# rename Responses to Response
for response_label in soup.find_all('h5', text='Responses'):
    response_label.string = 'Response payload'

# example body after response payload tag
for example in soup.find_all('pre', attrs={'class': 'example'}):
    response_payload_tag = example.find_next_sibling('h5', attrs={'class': 'field-label'})
    response_payload_tag.insert_after(example)

# change text for status codes
status_codes = ['200', '201']
for code in status_codes:
    for status_code_tag in soup.find_all('h5', attrs={'class': 'field-label'}, text=code):
        status_code_name = status_code_tag.next_sibling
        status_code_tag.string = f"Status code: {code} -"
        status_code_tag.append(status_code_name)

# remove request body model name
for request_body_tag in soup.find_all('h5', text='Request body'):
    field_items = request_body_tag.find_next_sibling('div')
    model = field_items.find('div', attrs={'class': 'model'})
    if model:
        model_copy = copy(model)
        field_items.div.extract()
        field_items.append(model)

# Write output file
with open('output.html', 'w') as f:
    body = soup.find('body')
    body = body.findChildren(recursive=False)
    body = str(body)[1:-1]

    # body = "".join(line.strip() for line in body.split("\n"))
    
    body = body.replace('This API call consumes the following media types via the  request header:', '')
    body = body.replace('response header.', '')
    body = body.replace('Controller', '')
    
    f.write(body)
