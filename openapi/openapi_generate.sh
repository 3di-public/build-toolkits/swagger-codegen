#!/bin/sh

# --------------
# 3di TechX - Keith Kirkwood
#
# Quick script to check for passed parameter and chain together the openAPI
# documentation generate and cleanup steps.
#
# --------------

if [ $# -eq 0 ]
then
  echo "ERROR: No argument supplied - you must supply an openAPI spec (as a URL or file)."
else
  # Verify that first passed parameter is a valid URL or file
  INPUT=$1
  if wget -q $INPUT
  then
      echo "INFO: Supplied input is retrievable as a URL."
  else
      if [ -f $INPUT ]
      then
          echo "INFO: Supplied input exists as a file."
      else
          echo "ERROR: Supplied input is not available as a URL or file - abort."
          exit 1
      fi
  fi

  # Generate the HTML from the passed spec - creates index.html
  java -jar $OGC_HOME/openapi-generator-cli.jar generate -g html -o output/ -i $@
  # Run cleanup script - expects index.html, creates output.html
  cd output
  python3 $OGC_HOME/openapi_cleanup.py

  # exit with success
  exit 0
fi
