# OpenAPI Generator

This repo builds a docker image based on [OpenAPI Generator CLI](https://openapi-generator.tech), and adds a python 3 environment for running [beautifulsoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/). 

* https://hub.docker.com/r/openapitools/openapi-generator-cli

This image holds a _openapi-generator.sh_ wrapper (available on the path and as the image ENTRYPOINT) which creates a sanitised HTML docs fragment.

When run, the container takes a single parameter of the input openAPI spec URL or file, and returns an error if the parameter appears to be neither. File input can be achieved either by bind mounting a local directory using the docker command line/docker-compose, or say a CI/CD environment which makes an appropriate file available in the container.

This creates a sanitised HTML docs fragment in an _output_ dir in the current directory, named _output.html_. This can then be used in, for example, a Hugo SSG site as follows:

<div>{{ readFile "output.html" | safeHTML }}</div>
