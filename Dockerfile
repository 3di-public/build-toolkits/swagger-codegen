FROM openapitools/openapi-generator-cli:v5.2.1
# Add python 3 environment for manipulation of codegen output
RUN apt update && apt-get install -y python3 python3-pip openssl
# Install any required modules (and update pip)
RUN pip3 install --no-cache-dir --upgrade pip bs4 html5lib
# Make some useful symlinks that are expected to exist
RUN cd /usr/bin && \
    ln -s pydoc3 pydoc && \
    ln -s python3 python && \
    ln -s python3-config python-config

WORKDIR /opt/openapi-generator/modules/openapi-generator-cli/target

# Add openapi cleanup script and shell wrapper, and set as executable
ADD openapi/openapi_cleanup.py  ./
ADD openapi/openapi_generate.sh  ./
RUN chmod +x ./openapi_generate.sh

# Set environment variable for openapi generator home
ENV OGC_HOME=/opt/openapi-generator/modules/openapi-generator-cli/target

# Add directory to path (to allow tidier CI scripts)
ENV PATH=$OGC_HOME:$PATH

ENTRYPOINT ["openapi_generate.sh"]
